
/* global require, console */

//Core includes
var util = require('util');
var fs = require('fs');

var process = require('process');
var clearapp = require('clearapp');
var changeCase = require('change-case');

var express = require('express');
var app = express();

//Get specific list
app.param('list', function (req, res, next, list) {
  clearapp.tasks(function(error, tasks) {
    res.send(tasks.filter(function(item) {
      return (changeCase.pascalCase(item.list_title) == changeCase.pascalCase(list));
    }).map(function(task) {
      return task.title;
    }));
  });
  next();
});


app.get('/:list', function (req, res) {
});

app.get('/', function (req, res) {
  clearapp.lists(function(error, lists) {
    res.send(lists.map(function(list) {
      return changeCase.pascalCase(list.title);
    }));
  });
});

var server = app.listen(7000, function() {
    console.log('Listening on port %d', server.address().port);
});
